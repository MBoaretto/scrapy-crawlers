import json
import scrapy

from tutorial.scrapy_twitter import TwitterStreamFilterRequest, to_item


class StreamFilterSpider(scrapy.Spider):
    name = "stream-filter"
    allowed_domains = ["twitter.com"]

    def __init__(self, track = None, *args, **kwargs):
        if not track:
            raise scrapy.exceptions.CloseSpider('Argument track not set.')
        super(StreamFilterSpider, self).__init__(*args, **kwargs)
        self.track = track.split(',')

    def start_requests(self):
        return [ TwitterStreamFilterRequest(track = self.track) ]

    def parse(self, response):
        tweets = response.tweets
        for tweet in tweets:

            if 'retweet_count' not in tweet:
                tweet.update({ "retweet_count":0})

            if 'favorite_count' not in tweet:
                tweet.update({ "favorite_count":0})

            item = to_item(tweet)

            print('----------------------')
            print(self.track)
            print(type(self.track))
            # print(item)
            print(type(item))
            print('======================')

            # aux = {k: item[k] for k in ('created_at',
            #                             'text',
            #                             'user',
            #                             'retweet_count',
            #                             'urls',
            #                             'favorite_count')}
            # for items in item:
            #     with open('stream.json', 'a') as fout:
            #         json.dump(dict(items), fout)
            #         fout.write("\n")
