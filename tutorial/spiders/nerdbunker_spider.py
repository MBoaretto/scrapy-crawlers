import scrapy
import json

class NerdBunkerSpider(scrapy.Spider):
    name = 'nerdbunker'

    start_urls = ['http://jovemnerd.com.br/nerdbunker/?search=&page=1']

    max_page = 5

    def parse(self, response):
        for article in response.css('article'):
            art = {
                    'title' : article.css('div h2 a::text').extract_first(),
                    'label' : article.css('div h3 a::text').extract_first(),
                    'detail' : article.css('div p a::attr(href)').extract_first(),
                }
            with open('nerdbunker.json', 'a') as fp:
                line = json.dumps(dict(art)) + "\n"
                fp.write(line)

        # follow pagination links
        url = response.url
        page = url.split("=")[-1]
        nxt_page = str(int(page)+1)
        if int(page) < self.max_page:
            yield response.follow('http://jovemnerd.com.br/nerdbunker/?search=&page='+nxt_page, callback=self.parse)
