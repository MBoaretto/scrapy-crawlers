import scrapy
import subprocess
from scrapy.settings import Settings
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

import settings as my_settings
from spiders.quotes_spider import QuotesSpider
from spiders.nerdbunker_spider import NerdBunkerSpider
from spiders.feedley_spider import Feedly_Craler

process = CrawlerProcess({'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'})

# feedly = Feedly_Craler()
# feedly.run_crawl()
subprocess.call(['scrapy', 'crawl', 'pixelwolfhq'])
subprocess.call(['scrapy', 'crawl', 'realDonaldTrump'])

# TODO stream-filter
# subprocess.call(['scrapy', 'crawl', 'stream-filter', '-a', 'track=#starwars'])

# process.crawl(QuotesSpider)
# process.crawl(NerdBunkerSpider)
# process.start()
